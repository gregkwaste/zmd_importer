import struct
import bpy,bmesh,io,os,imp,math
from mathutils import Vector,Matrix

#Functions
def read_uvs_f(f):
    return struct.unpack('<2f',f.read(8))

def read_vertices_f(f):
    return struct.unpack('<3f',f.read(12))

def facereadstrip(f, count, length):
    faces = []
    indiceslength = length
    facecount = count - 2
    tdict = {2:'H',4:'I'}
    string = '<3' + tdict[length]

    flag = False
    for i in range(facecount):
        back = f.tell()
        temp = struct.unpack(string, f.read(indiceslength * 3))
        if not (temp[0] == temp[1] or temp[1] == temp[2] or temp[0] == temp[2]):
            if flag is False:
                faces.append((temp[0], temp[1], temp[2]))
            else:
                faces.append((temp[2], temp[1], temp[0]))
            flag = not flag
        f.seek(back + indiceslength)
    
    f.seek(indiceslength*2,1)
    return faces

def read_string_1(f):
    c = ''
    for i in range(128):
        s = struct.unpack('B', f.read(1))[0]
        if not s:
            return c
        else:
            c += chr(s)


#Mesh Creation
#Create Mesh and Object Function
def createmesh(verts,faces,uvs,name,count,id,subname,colors,normal_flag,normals,location):
    scn=bpy.context.scene
    mesh=bpy.data.meshes.new("mesh"+str(count))
    mesh.from_pydata(verts,[],faces)
    

    
    
    for i in range(len(uvs)):
        uvtex=mesh.uv_textures.new(name='map'+str(i))
    for i in range(len(colors)):
        coltex=mesh.vertex_colors.new(name='col'+str(i))
    
        
        
    bm=bmesh.new()
    bm.from_mesh(mesh)
    
    #Create UV MAPS
    for i in range(len(uvs)):
        uvlayer=bm.loops.layers.uv['map'+str(i)]
        for f in bm.faces:
            for l in f.loops:
                l[uvlayer].uv.x=uvs[i][l.vert.index][0]
                l[uvlayer].uv.y=1-uvs[i][l.vert.index][1]
    

    #Create VERTEX COLOR MAPS
    for i in range(len(colors)):
        collayer=bm.loops.layers.color['col'+str(i)]
        for f in bm.faces:
                for l in f.loops:
                        l[collayer].r=colors[i][l.vert.index][0]
                        l[collayer].g=colors[i][l.vert.index][1]
                        l[collayer].b=colors[i][l.vert.index][2]
    
    #Pass Normals
    if normal_flag==True:
        for i in range(len(normals)):
            bm.verts[i].normal=normals[i]
     

    # Finish up, write the bmesh back to the mesh
    bm.to_mesh(mesh)
    bm.free()  # free and prevent further access
    
    
    object=bpy.data.objects.new(name+'_'+str(id)+'_'+str(count),mesh)
    
    object.location=location
    
    #Transformation attributes inherited from bounding boxes
    #if not name=='stadium': 
        #object.scale=Vector((0.1,0.1,0.1))
        #object.rotation_euler=Euler((1.570796251296997, -0.0, 0.0), 'XYZ')
    bpy.context.scene.objects.link(object)
    
    return object.name



#Default File
#f=open('C:\\Users\\gkass_000\\Downloads\\dave mirra\\Dave Mirra Freestyle BMX\\assets\\bike.zmd','rb')
#f=open('C:\\Users\\gkass_000\\Downloads\\dave mirra\\Dave Mirra Freestyle BMX\\assets\\bike_hires.zmd','rb')
f=open('C:\\Users\\gkass_000\\Downloads\\dave mirra\\Dave Mirra Freestyle BMX\\assets\\AmishBoy1.zmd','rb')

#Parsing MODL Header
f.seek(0x10)
# Parsing MLDF Header
f.seek(0x4,1)
objNum = struct.unpack('<I', f.read(4))[0]-1
f.seek(0x8,1)
#Parsing MDFF Header
f.seek(0x34,1)
#First Section Offset
off = struct.unpack('<I',f.read(4))[0]
f.seek(4,1) # Skipping probably type
#Getting Bone Count
count = struct.unpack('<I',f.read(4))[0]
boneIds=[]
boneMatrices=[]
boneLocations=[]
for i in range(count):
    t = Matrix()
    for j in range(3):
        for k in range(3):
            t[j][k] = struct.unpack('<f',f.read(4))[0]
    
    boneLocations.append(Vector(struct.unpack('<3f',f.read(12))))
    f.seek(8,1)
    boneMatrices.append(t)            

# print(boneLocations)
#f.seek(off - 4,1)
for i in range(count):
    f.seek(0x20,1)


#Parsing Models
for j in range(objNum):

    #Reached First Model
    f.seek(4,1) #Skipping 0x14000000
    secSize = struct.unpack('<I',f.read(4))[0]
    f.seek(4,1)
    # Storing Next Offset
    nextObject = f.tell()+secSize
    f.seek(0x34,1) #Skipping to Vertex Number
    iNum = struct.unpack('<I', f.read(4))[0] * 4
    vNum = struct.unpack('<I', f.read(4))[0]
    f.seek(0x10,1)

    uvs = []
    uvs_top=[]
    indices=[]
    verts = []
    for i in range(vNum):
        uvs.append(read_uvs_f(f))
    for i in range(vNum):
        uvs.append(uvs[i])
    
    uvs_top.append(uvs)
    indices = facereadstrip(f, iNum, 2)

    #Parsing Vertices Section
    f.seek(0x18,1)
    for i in range(2*vNum):
        verts.append(read_vertices_f(f))
        #f.read(0xC)


    #Storing Position
    f.seek(4,1)
    nextPos = struct.unpack('<I',f.read(4))[0] + f.tell() + 4
    f.seek(0x4, 1)
    # Getting Mesh Name
    
    #New Group
    f.seek(4,1)
    f.seek(struct.unpack('<I',f.read(4))[0]+4, 1)
    #New Group Just Pass Through
    f.seek(0xC,1)
    #New Group
    f.seek(4,1)
    f.seek(struct.unpack('<I',f.read(4))[0]+4, 1)
    
    #Get Name
    f.seek(0x28, 1) # Skip to object name
    name = read_string_1(f)


    # Testing Second Section
    f.seek(nextPos)
    f.seek(0x20,1)
    #print(hex(f.tell()))
    iNum2 = struct.unpack('<I',f.read(4))[0]
    f.seek(0x8,1) # Skipping some unknowns
    indices2 = facereadstrip(f, iNum2, 4)
    print(indices2,vNum)
    
    createmesh(verts, indices2, uvs_top, name, j ,0,'',[],False,[],boneLocations[1+j])
    print('Ending at: ',hex(f.tell()))
    f.seek(nextObject)
    print('Current Object: ',name, 'Next Offset: ', hex(f.tell()))


f.close()


