import struct

#Functions
def read_uvs_f(f):
    return struct.unpack('<2f',f.read(8))

def read_vertices_f(f):
    return struct.unpack('<3f',f.read(12))

def facereadstrip(f, count):
    faces = []
    indiceslength = 2
    facecount = count - 2
    string = '<3H'

    flag = False
    for i in range(facecount):
        back = f.tell()
        temp = struct.unpack(string, f.read(indiceslength * 3))
        if not (temp[0] == temp[1] or temp[1] == temp[2] or temp[0] == temp[2]):
            if flag is False:
                faces.append((temp[0], temp[1], temp[2]))
            else:
                faces.append((temp[2], temp[1], temp[0]))
            flag = not flag
        f.seek(back + indiceslength)
    
    f.seek(indiceslength*2,1)
    return faces

#Default File
f=open('C:\\Users\\gkass_000\\Downloads\\dave mirra\\Dave Mirra Freestyle BMX\\assets\\bike.zmd','rb')

#Parsing MODL Header
f.seek(0x10)
# Parsing MLDF Header
f.seek(0x10,1)
#Parsing MDFF Header
f.seek(0x34,1)
#First Section Offset
off = struct.unpack('<I',f.read(4))[0]
f.seek(4,1) # Skipping probably type
count = struct.unpack('<I',f.read(4))[0]
# Count is probably bone count
f.seek(off - 4,1)
for i in range(count):
    f.seek(0x20,1)


#Reached First Model
f.seek(4,1) #Skipping 0x14000000
secSize = struct.unpack('<I',f.read(4))[0]
f.seek(4,1)
# Storing Next Offset
nextOffset = f.tell()+secSize
f.seek(0x34,1) #Skipping to Vertex Number
iNum = struct.unpack('<I', f.read(4))[0] * 4
vNum = struct.unpack('<I', f.read(4))[0]
f.seek(0x10,1)

uvs = []
indices=[]
verts = []
for i in range(vNum):
    uvs.append(read_uvs_f(f))

indices = facereadstrip(f,iNum)

#Parsing Vertices Section
print hex(f.tell())
f.seek(0x18,1)
for i in range(vNum):
    verts.append(read_vertices_f(f))
    f.read(0xC)

#for i in uvs: print i
#for i in indices: print i

#for i in verts: print i

print vNum, iNum
print hex(f.tell())

f.seek(nextOffset)
print hex(f.tell())


